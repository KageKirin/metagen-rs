# Metagen-rs

Unity `.meta` file generator written in Rust.

Doesn't try to mimick Unity's GUID generator in favor of stable GUIDs.
Depends on xxHash for hashing.
